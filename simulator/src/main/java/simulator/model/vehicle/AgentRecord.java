package simulator.model.vehicle;

public class AgentRecord {
    private long id;
    private String action;

    public AgentRecord(long id, String action) {
        this.id = id;
        this.action = action;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
