package simulator.model.vehicle;

import java.util.ArrayList;
import java.util.List;

public class TimeWindow {
    private int step;
    private List<AgentRecord> records = new ArrayList<>();

    public TimeWindow(int step) {
        this.step = step;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public List<AgentRecord> getRecords() {
        return records;
    }

    public void setRecords(List<AgentRecord> records) {
        this.records = records;
    }

    
}
