package simulator.model.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import simulator.model.vehicle.AgentRecord;
import simulator.model.vehicle.RoundTrip;
import simulator.model.vehicle.TimeWindow;

public class InformationView {

    private List<String> crashLog = new ArrayList<>();
    private List<RoundTrip> roundTrips = new ArrayList<>();
    private Map<Integer,TimeWindow> timeWindows = new TreeMap<>();
    private List<Integer> steps = new ArrayList<>();

    public InformationView() {
    }

    public void addCrashLog(String message){
        crashLog.add(message);
    }

    public void addTrip(RoundTrip trip){
        roundTrips.add(trip);
    }

    public List<String> getCrashLog() {
        return crashLog;
    }

    public void setCrashLog(List<String> crashLog) {
        this.crashLog = crashLog;
    }

    public List<RoundTrip> getRoundTrips() {
        return roundTrips;
    }

    public void setRoundTrips(List<RoundTrip> roundTrips) {
        this.roundTrips = roundTrips;
    }

    public void createTimeWindow(int step) {
        steps.add(step);
        timeWindows.put(step, new TimeWindow(step));
        if (steps.size()>10) {
            int oldStep = steps.remove(0);
            timeWindows.remove(oldStep);
        }
    }

    public void logAgentAction(int step, long vehicleId, String action) {
        TimeWindow timeWindow = timeWindows.get(step);
        timeWindow.getRecords().add(new AgentRecord(vehicleId, action));
    }

    public Map<Integer, TimeWindow> getTimeWindows() {
        return timeWindows;
    }

    public int getMaxStep() {
        return steps.get(steps.size()-1);
    }

    public List<Integer> getSteps() {
        return steps;
    }
}
