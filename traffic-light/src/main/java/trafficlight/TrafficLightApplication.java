package trafficlight;

import java.net.URI;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import net.minidev.json.JSONObject;
import trafficlight.service.TrafficLightService;

@SpringBootApplication
public class TrafficLightApplication {
    @Value("${clock.host}")
    private String clockHost;
    
    @Value("${traffic.host}")
    private String trafficHost;
    
    public static void main(String[] args) {
        SpringApplication.run(TrafficLightApplication.class, args);
    }

    @Bean
    CommandLineRunner demo(TrafficLightService service) {
        return args -> {
            JSONObject registration = new JSONObject();
            registration.put("name", "traffic-light");
            registration.put("uri", "http://"+trafficHost+":8082/step");

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            RestTemplate template = new RestTemplate();
            URI uri = new URI("http://"+clockHost+":9000/registry");
            HttpEntity<String> body = new HttpEntity<>(registration.toString(), headers);
            template.postForEntity(uri, body, String.class);
        };
    }
}
