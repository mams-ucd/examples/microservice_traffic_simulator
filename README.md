# Example System: Microservices-based Traffic Simulator

This project is a prototype of an Agent-Based Simulation that has been implmented 
as a microservices architecture where the agents are implmented using the [MAMS Architectural Style](/mams-ucd/mams-cartago). The prototype aims to demonstrate how microservices can be used to create hybrid agent-based simulations. To this end, the prototype consists of 3 simulators:

- **Home Simulator:** The home simulator is a simplistic simulation of a household environment.  The current implementation, which can be found in the [home-simulator](home-simulator) project is very much a placeholder. Currently, all an agent is able to do is watch tv or leave.

- **Work Simulator:** The work simulator is a simplistic simulation of a work environment.  The current implementation, which can be found in the [work-simulator](work-simulator) project allows agents to "work" or leave.

- **Traffic Simulator:** The traffic simulator is the most complex of the 3 simulators. It supports the implementation of a traffic network with traffic lights. Agents drive cars around the environment travelling from home to work and vice versa. The current implementation of the traffic simulation can be found in the [simulator](simulator) project.

## Requirements

To run this prototype, you need the following:

- Java JDK 1.8 (or higher)
- Maven (3.9.0 or higher)
- Docker Desktop

The example has been tested against the latest version of Docker Desktop and uses Docker Compose v2.

## Running the Demo

Deploying the example application involves multiple stages:

1. Create a Docker Network by typing: `docker network create data`

2. Start up Neo4J via a terminal by going to folder `/neo4j` and typing `docker compose up -d`.

3. Compile all the projects by typing `mvn package` in the project root folder.

4. Start up the clock, simulators and web interface by typing `docker compose up --build -d` in the project root. You can verify that the service is running by typing `http://localhost:8080/` into a browser.

5. Start up the simulation by going to folder `/management` and typing `docker compose up -d` to run the management service.  This service loads the example configuration, initialises the simulations and creates the relevant agents. If you look at the page you loaded in the browser on the previous step, it should now look something like this: ![screenshot](images/screenshot.png)

## Using the Java drivers

By default, the system uses the ASTRA agent deployment. The implementation of these agents can be found in the [drivers](/drivers) project.

To switch to the Java deployment alter the docker-compose.yml file in the root folder:

1. Comment out the AGENT_HOST line that refers to `drivers-web` and incomment the line that refers to `drivers`.

2. Comment out the `drivers-web` service and uncomment the `drivers` service.

## Understanding the 