package management;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import management.service.ConfigurationService;
import net.minidev.json.JSONObject;

@SpringBootApplication
public class ManagementApplication {
    @Value("${clock.host}")
    private String clockHost;
    
    @Value("${simulator.host}")
    private String simulationHost;

    @Value("${traffic.host}")
    private String trafficHost;

    @Value("${management.host}")
    private String managementHost;

    @Value("${home.host}")
    private String homeHost;

    @Value("${work.host}")
    private String workHost;

    @Value("${agent.host}")
    private String agentHost;

    public static void main(String[] args) {
            SpringApplication.run(ManagementApplication.class, args);
    }

    @Bean
    CommandLineRunner initial(@Autowired ConfigurationService configurationService) {
        return args -> {
            JSONObject registration = new JSONObject();
            registration.put("name", "management");
            registration.put("uri", "http://"+managementHost+":8083/step");

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            RestTemplate template = new RestTemplate();
            URI uri = new URI("http://"+clockHost+":9000/registry");
            HttpEntity<String> body = new HttpEntity<>(registration.toString(), headers);
            template.postForEntity(uri, body, String.class);

            System.out.println("First URL: http://"+simulationHost+":8081/setup");
            configurationService.sendConfiguration("/traffic.json", "http://"+simulationHost+":8081/setup");
            configurationService.sendConfiguration("/home.json", "http://"+homeHost+":8084/setup");
            configurationService.sendConfiguration("/work.json", "http://"+workHost+":8085/setup");
            configurationService.sendConfiguration("/drivers.json", "http://"+agentHost+":9001/main/setup", "PUT");
        };
    }

}
