class WebSocket{
    constructor(endPoint, port){
        this.endPoint = endPoint;
        this.stompClient = null;
        this.data = null;
        this.port = port;
        this.funct=function(that, messageOutput) {
            that.data = JSON.parse(messageOutput.body);
        };
    }
    setFunct(fn) {
        this.funct = fn;
    }

    connect() {
        var that = this;
        var socket = new SockJS('http://localhost:'+this.port+'/'+this.endPoint);
        this.stompClient = Stomp.over(socket);
        this.stompClient.connect({}, function(frame) {
            console.log('Connected: '+that.endPoint+ ' ' + frame);
            that.stompClient.subscribe('/topic/'+that.endPoint, function(messageOutput) {
                that.funct(that, messageOutput);
            });
        });
    }
    
    disconnect() {
        if(this.stompClient != null) {
            this.stompClient.disconnect();
        }
        setConnected(false);
        console.log("Disconnected");
    }

    sendMessage() {
        this.stompClient.send("/app/"+this.endPoint, {}, null);
    }
}