var TIMEFILTER = -1;
var AGENTFILTER = -1;

infoWS = new WebSocket('information', 8081),

infoWS.setFunct(function(that, messageOutput){
    data = JSON.parse(messageOutput.body);
    let info = $("#info");
    info.empty();

    $('#cars').empty();
    $('#time').html(data.maxStep);

    if (data.steps.length > 0) {
        timeWindow = data.timeWindows[""+(data.steps[data.steps.length-1])];
        $('#agent_filter').empty();
        $('#agent_filter').append("<option value='-1'>All Agents</option>");

        for (let i=0;i<timeWindow.records.length;i++) {
            id = timeWindow.records[i].id;
            $('#agent_filter').append("<option value='"+id+"'>Agent "+id+"</option>");
        }
    }

    for(var i = 0; i < data.roundTrips.length; i++ ){
        let roundTrip = data.roundTrips[i];
        info.append("<p>[Vehicle with Id "+roundTrip.vehicleId + " took " + roundTrip.totalTime+"s from "+roundTrip.route.startNode+" to "+roundTrip.route.endNode+"]</p>");
    }
    for(let i = 0; i < data.crashLog.length; i++ ){
        info.append("<p>"+data.crashLog[i]+"</p>");
    }

    let start = 0;
    if (TIMEFILTER > -1) {
        start = data.steps.length-TIMEFILTER;
        if (start < 0) start = 0;
    }

    for(let i = start; i < data.steps.length; i++ ){
        let step = data.steps[i];
        $("#cars").append("<p>STEP: " + step+"</p>");
        let timeWindow = data.timeWindows[""+step];
        for (let j=0; j < timeWindow.records.length; j++) {
            if (AGENTFILTER == -1 || timeWindow.records[j].id == AGENTFILTER) 
                $("#cars").append("<p>Agent " + timeWindow.records[j].id + " performed action: " + timeWindow.records[j].action +"</p>");
        }
    }
});
infoWS.connect();

function setTimeFilter() {
    TIMEFILTER=$('#time_filter').val()
}
function setAgentFilter() {
    AGENTFILTER=$('#agent_filter').val()
}